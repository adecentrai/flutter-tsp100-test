import 'package:flutter/material.dart';
import 'dart:ui' as ui;
import './XD_Home.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_star_prnt/flutter_star_prnt.dart';

class XD_Splash extends StatelessWidget {
  XD_Splash({
    Key key,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xffffffff),
      body: Stack(
        children: <Widget>[
          // Adobe XD layer: 'Rectangle' (shape)
          Container(
              width: 414.0, height: 896.0, color: const Color(0xff6886c5)),
          // Adobe XD layer: '14155' (group)
          Stack(
            children: <Widget>[
              // Adobe XD layer: 'Mask' (shape)
              Container(
                  width: 414.0, height: 896.0, color: const Color(0xff6886c5)),
              // Adobe XD layer: '14155' (group)
              Stack(
                children: <Widget>[
                  Transform.translate(
                    offset: Offset(-645.0, -4.0),
                    child:
                        // Adobe XD layer: '14155' (shape)
                        Container(
                      width: 1350.0,
                      height: 900.0,
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          image: const AssetImage(''),
                          fit: BoxFit.fill,
                        ),
                      ),
                    ),
                  ),
                  // Adobe XD layer: 'Mask' (shape)
                  Container(
                      width: 414.0,
                      height: 896.0,
                      color: const Color(0xff6886c5)),
                ],
              ),
            ],
          ),
          // Adobe XD layer: 'Rectangle' (shape)
          Container(
              width: 414.0, height: 896.0, color: const Color(0xab000000)),
          Transform.translate(
            offset: Offset(156.0, 268.5),
            child:
                // Adobe XD layer: 'Book (2)' (group)
                SvgPicture.string(
              _shapeSVG_998d308f84db47cab67457183244f9ce,
              allowDrawingOutsideViewBox: true,
            ),
          ),
          Transform.translate(
            offset: Offset(57.0, 623.0),
            child:
                // Adobe XD layer: 'BTN' (group)
                Stack(
              children: <Widget>[
                // Adobe XD layer: 'Rectangle' (shape)
                Container(
                  width: 300.0,
                  height: 60.0,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10.0),
                    color: const Color(0xffffffff),
                  ),
                ),
                Transform.translate(
                  offset: Offset(71.5, 18.0),
                  child:
                      // Adobe XD layer: 'Lets Get Started!' (text)
                      GestureDetector(onTap: (){
                        Navigator.of(context).push(MaterialPageRoute(builder: (context) => XD_Home()));

                      },child:SizedBox(
                        width: 158.0,
                        child: Text(
                          'Lets Get Started!',
                          style: TextStyle(
                            fontFamily: 'Manrope-Bold',
                            fontSize: 19,
                            color: const Color(0xff3a3a3a),
                            letterSpacing: -0.443333345413208,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      )
                  ),
                ),
              ],
            ),
          ),
          Transform.translate(
            offset: Offset(121.5, 432.0),
            child:
                // Adobe XD layer: 'Educators' (text)
                SizedBox(
              width: 172.0,
              child: Text(
                'Educators',
                style: TextStyle(
                  fontFamily: 'Manrope-Bold',
                  fontSize: 35,
                  color: const Color(0xffffffff),
                  letterSpacing: -0.8166666889190675,
                ),
                textAlign: TextAlign.center,
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(41.0, 495.0),
            child:
                // Adobe XD layer: 'A brand new experien' (text)
                SizedBox(
              width: 332.0,
              height: 69.0,
              child: SingleChildScrollView(
                  child: Text(
                'A brand new experience to learn something new. Polish your skills  with \nthis app. Easy to use.',
                style: TextStyle(
                  fontFamily: 'Manrope-Regular',
                  fontSize: 18,
                  color: const Color(0xffffffff),
                  letterSpacing: -0.4200000114440918,
                ),
                textAlign: TextAlign.center,
              )),
            ),
          ),
        ],
      ),
    );
  }
}

const String _shapeSVG_998d308f84db47cab67457183244f9ce =
    '<svg viewBox="156.0 268.5 102.0 98.0" ><g transform="translate(156.0, 268.0)"><path transform="translate(8.0, 0.5)" d="M 37.11579132080078 95.98488616943359 L 0 82.33721160888672 L 0 0 L 37.11579132080078 13.64767456054688 C 40.84999847412109 15.0011625289917 45.03684234619141 15.0011625289917 48.88420867919922 13.64767456054688 L 86 0 L 86 82.33721160888672 L 48.88420867919922 95.98488616943359 C 45.03684234619141 97.33837127685547 40.96315765380859 97.33837127685547 37.11579132080078 95.98488616943359 Z" fill="none" stroke="#ffffff" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" /><path transform="translate(51.0, 15.5)" d="M 0.5 55 L 0.5 0" fill="none" stroke="#ffe0ac" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" /><path transform="translate(51.0, 81.5)" d="M 0.5 3 L 0.5 0" fill="none" stroke="#ffe0ac" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" /><path transform="translate(0.0, 10.5)" d="M 94.18000030517578 0.9040128588676453 L 102 0 L 102 82.49117279052734 L 52.92666625976563 87.91524505615234 C 51.68000030517578 88.02825164794922 50.43333435058594 88.02825164794922 49.18666839599609 87.91524505615234 L 0 82.49117279052734 L 0 0 L 7.820000171661377 0.9040128588676453" fill="none" stroke="#ffe0ac" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" /></g></svg>';
