import 'package:flutter/material.dart';
import 'dart:ui' as ui;
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_star_prnt/flutter_star_prnt.dart';


class XD_Home extends StatelessWidget {
  XD_Home({
    Key key,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xffffffff),
      body: Stack(
        children: <Widget>[
          // Adobe XD layer: 'Background' (shape)
          Container(
            width: 414.0,
            height: 896.0,
            decoration: BoxDecoration(
              color: const Color(0xfffafafb),
            ),
          ),
          Transform.translate(
            offset: Offset(32.0, 338.0),
            child:
                // Adobe XD layer: 'Card' (group)
                Stack(
              children: <Widget>[
                // Adobe XD layer: 'Rectangle' (shape)
                Container(
                  width: 221.0,
                  height: 257.0,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15.0),
                    color: const Color(0xffffffff),
                    border:
                        Border.all(width: 1.0, color: const Color(0x266886c5)),
                    boxShadow: [
                      BoxShadow(
                          color: const Color(0x338a959e),
                          offset: Offset(0, 30),
                          blurRadius: 60)
                    ],
                  ),
                ),
                Transform.translate(
                  offset: Offset(13.0, 168.0),
                  child:
                      // Adobe XD layer: '50 Students Enrolled' (text)
                      Text(
                    '50 Students Enrolled',
                    style: TextStyle(
                      fontFamily: 'Manrope-Regular',
                      fontSize: 13,
                      color: const Color(0xff8a959e),
                      letterSpacing: -0.6999999923706055,
                    ),
                    textAlign: TextAlign.left,
                  ),
                ),
                Transform.translate(
                  offset: Offset(7.0, 6.0),
                  child:
                      // Adobe XD layer: 'Bitmap' (group)
                      Stack(
                    children: <Widget>[
                      // Adobe XD layer: 'Mask' (shape)
                      Container(
                        width: 207.0,
                        height: 121.0,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(15.0),
                          color: const Color(0xffd8d8d8),
                        ),
                      ),
                      // Adobe XD layer: 'Bitmap' (group)
                      Stack(
                        children: <Widget>[
                          Transform.translate(
                            offset: Offset(-7.0, -13.0),
                            child:
                                // Adobe XD layer: 'Bitmap' (shape)
                                Container(
                              width: 222.0,
                              height: 148.0,
                              decoration: BoxDecoration(
                                image: DecorationImage(
                                  image: const AssetImage(''),
                                  fit: BoxFit.fill,
                                ),
                              ),
                            ),
                          ),
                          // Adobe XD layer: 'Mask' (shape)
                          Container(
                            width: 207.0,
                            height: 121.0,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(15.0),
                              color: const Color(0xffd8d8d8),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                Transform.translate(
                  offset: Offset(13.0, 142.0),
                  child:
                      // Adobe XD layer: 'Learning Photoshop C' (text)
                      Text(
                    'Learning Photoshop CC',
                    style: TextStyle(
                      fontFamily: 'Manrope-Bold',
                      fontSize: 15,
                      color: const Color(0xff23374d),
                      letterSpacing: -0.7000000190734864,
                    ),
                    textAlign: TextAlign.left,
                  ),
                ),
                Transform.translate(
                  offset: Offset(15.0, 205.0),
                  child:
                      // Adobe XD layer: 'Rectangle' (shape)
                      Container(
                    width: 191.0,
                    height: 37.0,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(6.0),
                      color: const Color(0xff6886c5),
                    ),
                  ),
                ),
                Transform.translate(
                  offset: Offset(67.5, 214.0),
                  child:
                      // Adobe XD layer: 'Enroll Now' (text)
                      GestureDetector(child:SizedBox(
                          width: 86.0,
                          child: Text(
                            'Enroll Now',
                            style: TextStyle(
                              fontFamily: 'Manrope-Bold',
                              fontSize: 15,
                              color: const Color(0xffffffff),
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ),
                        onTap: () async {
                          List<PortInfo> list =
                          await StarPrnt.portDiscovery(StarPortType.All);
                          print(list);
                          list.forEach((port) async {
                            print(port.portName);
                            if (port.portName.isNotEmpty) {
                              print(await StarPrnt.checkStatus(
                                  portName: port.portName,
                                  emulation: 'StarGraphic',
                                )
                              );

                              PrintCommands commands = PrintCommands();
                              Map<String, dynamic> rasterMap = {
                                'appendBitmapText': "        Star Clothing Boutique\n" +
                                    "             123 Star Road\n" +
                                    "           City, State 12345\n" +
                                    "\n" +
                                    "Date:MM/DD/YYYY          Time:HH:MM PM\n" +
                                    "--------------------------------------\n" +
                                    "SALE\n" +
                                    "SKU            Description       Total\n" +
                                    "300678566      PLAIN T-SHIRT     10.99\n" +
                                    "300692003      BLACK DENIM       29.99\n" +
                                    "300651148      BLUE DENIM        29.99\n" +
                                    "300642980      STRIPED DRESS     49.99\n" +
                                    "30063847       BLACK BOOTS       35.99\n" +
                                    "\n" +
                                    "Subtotal                        156.95\n" +
                                    "Tax                               0.00\n" +
                                    "--------------------------------------\n" +
                                    "Total                           156.95\n" +
                                    "--------------------------------------\n" +
                                    "\n" +
                                    "Charge\n" +
                                    "156.95\n" +
                                    "Visa XXXX-XXXX-XXXX-0123\n" +
                                    "Refunds and Exchanges\n" +
                                    "Within 30 days with receipt\n" +
                                    "And tags attached\n",
                              };
                              commands.push(rasterMap);
                              print(await StarPrnt.print(
                                  portName: port.portName,
                                  emulation: 'StarGraphic',
                                  printCommands: commands));
                            }
                          });
                        },
                      ),
                ),
                Transform.translate(
                  offset: Offset(152.28, 145.0),
                  child:
                      // Adobe XD layer: 'Group' (group)
                      SvgPicture.string(
                    _shapeSVG_0e548a8677b942519d2eb7923e51fce9,
                    allowDrawingOutsideViewBox: true,
                  ),
                ),
                Transform.translate(
                  offset: Offset(152.0, 172.0),
                  child:
                      // Adobe XD layer: 'Group' (group)
                      Stack(
                    children: <Widget>[
                      // Adobe XD layer: 'emojione-star' (group)
                      Stack(
                        children: <Widget>[
                          // Adobe XD layer: 'ViewBox' (shape)
                          Container(
                            width: 9.0,
                            height: 9.0,
                            decoration: BoxDecoration(),
                          ),
                        ],
                      ),
                      Transform.translate(
                        offset: Offset(12.0, 0.0),
                        child:
                            // Adobe XD layer: 'emojione-star' (group)
                            Stack(
                          children: <Widget>[
                            // Adobe XD layer: 'ViewBox' (shape)
                            Container(
                              width: 9.0,
                              height: 9.0,
                              decoration: BoxDecoration(),
                            ),
                          ],
                        ),
                      ),
                      Transform.translate(
                        offset: Offset(25.0, 0.0),
                        child:
                            // Adobe XD layer: 'emojione-star' (group)
                            Stack(
                          children: <Widget>[
                            // Adobe XD layer: 'ViewBox' (shape)
                            Container(
                              width: 9.0,
                              height: 9.0,
                              decoration: BoxDecoration(),
                            ),
                          ],
                        ),
                      ),
                      Transform.translate(
                        offset: Offset(37.0, 0.0),
                        child:
                            // Adobe XD layer: 'emojione-star' (group)
                            Stack(
                          children: <Widget>[
                            // Adobe XD layer: 'ViewBox' (shape)
                            Container(
                              width: 9.0,
                              height: 9.0,
                              decoration: BoxDecoration(),
                            ),
                          ],
                        ),
                      ),
                      Transform.translate(
                        offset: Offset(49.0, 0.0),
                        child:
                            // Adobe XD layer: 'emojione-star' (group)
                            Stack(
                          children: <Widget>[
                            // Adobe XD layer: 'ViewBox' (shape)
                            Container(
                              width: 9.0,
                              height: 9.0,
                              decoration: BoxDecoration(),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Transform.translate(
            offset: Offset(32.0, 670.0),
            child:
                // Adobe XD layer: 'Vertical Card' (group)
                Stack(
              children: <Widget>[
                // Adobe XD layer: 'Rectangle' (shape)
                Container(
                  width: 350.0,
                  height: 79.0,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15.0),
                    color: const Color(0xffffffff),
                    border:
                        Border.all(width: 1.0, color: const Color(0x266886c5)),
                    boxShadow: [
                      BoxShadow(
                          color: const Color(0x338a959e),
                          offset: Offset(0, 30),
                          blurRadius: 60)
                    ],
                  ),
                ),
                Transform.translate(
                  offset: Offset(11.0, 9.0),
                  child:
                      // Adobe XD layer: 'Bitmap' (group)
                      Stack(
                    children: <Widget>[
                      // Adobe XD layer: 'Mask' (shape)
                      Container(
                        width: 61.0,
                        height: 61.0,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(11.0),
                          color: const Color(0xffd8d8d8),
                        ),
                      ),
                      // Adobe XD layer: 'Bitmap' (group)
                      Stack(
                        children: <Widget>[
                          Transform.translate(
                            offset: Offset(-26.0, 0.0),
                            child:
                                // Adobe XD layer: 'Bitmap' (shape)
                                Container(
                              width: 87.0,
                              height: 61.0,
                              decoration: BoxDecoration(
                                image: DecorationImage(
                                  image: const AssetImage(''),
                                  fit: BoxFit.fill,
                                ),
                              ),
                            ),
                          ),
                          // Adobe XD layer: 'Mask' (shape)
                          Container(
                            width: 61.0,
                            height: 61.0,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(11.0),
                              color: const Color(0xffd8d8d8),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                Transform.translate(
                  offset: Offset(83.0, 15.0),
                  child:
                      // Adobe XD layer: 'Learning UI/UX Conce' (text)
                      Text(
                    'Learning UI/UX Concept',
                    style: TextStyle(
                      fontFamily: 'Manrope-Bold',
                      fontSize: 16,
                      color: const Color(0xff3a3a3a),
                      letterSpacing: -0.56,
                    ),
                    textAlign: TextAlign.left,
                  ),
                ),
                Transform.translate(
                  offset: Offset(83.0, 42.0),
                  child:
                      // Adobe XD layer: '250 Students Enrolle' (text)
                      Text(
                    '250 Students Enrolled',
                    style: TextStyle(
                      fontFamily: 'Manrope-Regular',
                      fontSize: 14,
                      color: const Color(0xff8a959e),
                      letterSpacing: -0.49000000000000005,
                    ),
                    textAlign: TextAlign.left,
                  ),
                ),
                Transform.translate(
                  offset: Offset(257.0, 40.0),
                  child:
                      // Adobe XD layer: 'Rectangle' (shape)
                      Container(
                    width: 80.0,
                    height: 29.0,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(6.0),
                      color: const Color(0xff6886c5),
                      boxShadow: [
                        BoxShadow(
                            color: const Color(0x4d6886c5),
                            offset: Offset(0, 2),
                            blurRadius: 4)
                      ],
                    ),
                  ),
                ),
                Transform.translate(
                  offset: Offset(266.5, 45.0),
                  child:
                      // Adobe XD layer: 'Join Now' (text)
                      SizedBox(
                    width: 62.0,
                    child: Text(
                      'Join Now',
                      style: TextStyle(
                        fontFamily: 'Manrope-Bold',
                        fontSize: 14,
                        color: const Color(0xffffffff),
                        letterSpacing: -0.7899999771118165,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
              ],
            ),
          ),
          Transform.translate(
            offset: Offset(32.0, 778.0),
            child:
                // Adobe XD layer: 'Vertical Card' (group)
                Stack(
              children: <Widget>[
                // Adobe XD layer: 'Rectangle' (shape)
                Container(
                  width: 350.0,
                  height: 79.0,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15.0),
                    color: const Color(0xffffffff),
                    border:
                        Border.all(width: 1.0, color: const Color(0x266886c5)),
                    boxShadow: [
                      BoxShadow(
                          color: const Color(0x338a959e),
                          offset: Offset(0, 30),
                          blurRadius: 60)
                    ],
                  ),
                ),
                Transform.translate(
                  offset: Offset(16.0, 9.0),
                  child:
                      // Adobe XD layer: 'Bitmap' (group)
                      Stack(
                    children: <Widget>[
                      // Adobe XD layer: 'Mask' (shape)
                      Container(
                        width: 61.0,
                        height: 61.0,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(11.0),
                          color: const Color(0xffd8d8d8),
                        ),
                      ),
                      // Adobe XD layer: 'Bitmap' (group)
                      Stack(
                        children: <Widget>[
                          Transform.translate(
                            offset: Offset(-5.0, 0.0),
                            child:
                                // Adobe XD layer: 'Bitmap' (shape)
                                Container(
                              width: 71.0,
                              height: 61.0,
                              decoration: BoxDecoration(
                                image: DecorationImage(
                                  image: const AssetImage(''),
                                  fit: BoxFit.fill,
                                ),
                              ),
                            ),
                          ),
                          // Adobe XD layer: 'Mask' (shape)
                          Container(
                            width: 61.0,
                            height: 61.0,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(11.0),
                              color: const Color(0xffd8d8d8),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                Transform.translate(
                  offset: Offset(88.0, 12.0),
                  child:
                      // Adobe XD layer: 'Learning Adobe Illus' (text)
                      Text(
                    'Learning Adobe Illustrator',
                    style: TextStyle(
                      fontFamily: 'Manrope-Bold',
                      fontSize: 16,
                      color: const Color(0xff3a3a3a),
                      letterSpacing: -0.56,
                    ),
                    textAlign: TextAlign.left,
                  ),
                ),
                Transform.translate(
                  offset: Offset(88.0, 39.0),
                  child:
                      // Adobe XD layer: '189 Students Enrolle' (text)
                      Text(
                    '189 Students Enrolled',
                    style: TextStyle(
                      fontFamily: 'Manrope-Regular',
                      fontSize: 14,
                      color: const Color(0xff8a959e),
                      letterSpacing: -0.49000000000000005,
                    ),
                    textAlign: TextAlign.left,
                  ),
                ),
                Transform.translate(
                  offset: Offset(257.0, 37.0),
                  child:
                      // Adobe XD layer: 'Rectangle' (shape)
                      Container(
                    width: 80.0,
                    height: 29.0,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(6.0),
                      color: const Color(0xff6886c5),
                      boxShadow: [
                        BoxShadow(
                            color: const Color(0x4d6886c5),
                            offset: Offset(0, 2),
                            blurRadius: 4)
                      ],
                    ),
                  ),
                ),
                Transform.translate(
                  offset: Offset(266.5, 42.0),
                  child:
                      // Adobe XD layer: 'Join Now' (text)
                      SizedBox(
                    width: 62.0,
                    child: Text(
                      'Join Now',
                      style: TextStyle(
                        fontFamily: 'Manrope-Bold',
                        fontSize: 14,
                        color: const Color(0xffffffff),
                        letterSpacing: -0.7899999771118165,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
              ],
            ),
          ),
          Transform.translate(
            offset: Offset(267.0, 338.0),
            child:
                // Adobe XD layer: 'Card' (group)
                Stack(
              children: <Widget>[
                // Adobe XD layer: 'Rectangle' (shape)
                Container(
                  width: 221.0,
                  height: 257.0,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15.0),
                    color: const Color(0xffffffff),
                    border:
                        Border.all(width: 1.0, color: const Color(0x266886c5)),
                  ),
                ),
                Transform.translate(
                  offset: Offset(13.0, 168.0),
                  child:
                      // Adobe XD layer: '15 Students Enrolled' (text)
                      Text(
                    '15 Students Enrolled',
                    style: TextStyle(
                      fontFamily: 'Manrope-Regular',
                      fontSize: 13,
                      color: const Color(0xff8a959e),
                      letterSpacing: -0.6999999923706055,
                    ),
                    textAlign: TextAlign.left,
                  ),
                ),
                Transform.translate(
                  offset: Offset(7.0, 6.0),
                  child:
                      // Adobe XD layer: 'Bitmap' (group)
                      Stack(
                    children: <Widget>[
                      // Adobe XD layer: 'Mask' (shape)
                      Container(
                        width: 207.0,
                        height: 121.0,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(15.0),
                          color: const Color(0xffd8d8d8),
                        ),
                      ),
                      // Adobe XD layer: 'Bitmap' (group)
                      Stack(
                        children: <Widget>[
                          Transform.translate(
                            offset: Offset(-7.0, -13.0),
                            child:
                                // Adobe XD layer: 'Bitmap' (shape)
                                Container(
                              width: 222.0,
                              height: 148.0,
                              decoration: BoxDecoration(
                                image: DecorationImage(
                                  image: const AssetImage(''),
                                  fit: BoxFit.fill,
                                ),
                              ),
                            ),
                          ),
                          Transform.translate(
                            offset: Offset(-7.0, 0.0),
                            child:
                                // Adobe XD layer: 'Bitmap' (shape)
                                Container(
                              width: 182.0,
                              height: 121.0,
                              decoration: BoxDecoration(
                                image: DecorationImage(
                                  image: const AssetImage(''),
                                  fit: BoxFit.fill,
                                ),
                              ),
                            ),
                          ),
                          // Adobe XD layer: 'Mask' (shape)
                          Container(
                            width: 207.0,
                            height: 121.0,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(15.0),
                              color: const Color(0xffd8d8d8),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                Transform.translate(
                  offset: Offset(13.0, 142.0),
                  child:
                      // Adobe XD layer: 'Learn Sketching and' (text)
                      Text(
                    'Learn Sketching and Art',
                    style: TextStyle(
                      fontFamily: 'Manrope-Bold',
                      fontSize: 15,
                      color: const Color(0xff23374d),
                      letterSpacing: -0.7000000190734864,
                    ),
                    textAlign: TextAlign.left,
                  ),
                ),
                Transform.translate(
                  offset: Offset(15.0, 205.0),
                  child:
                      // Adobe XD layer: 'Rectangle' (shape)
                      Container(
                    width: 191.0,
                    height: 37.0,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(6.0),
                      color: const Color(0xff6886c5),
                    ),
                  ),
                ),
                Transform.translate(
                  offset: Offset(67.5, 214.0),
                  child:
                      // Adobe XD layer: 'Enroll Now' (text)
                      SizedBox(
                    width: 86.0,
                    child: Text(
                      'Enroll Now',
                      style: TextStyle(
                        fontFamily: 'Manrope-Bold',
                        fontSize: 15,
                        color: const Color(0xffffffff),
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
              ],
            ),
          ),
          Transform.translate(
            offset: Offset(32.0, 205.0),
            child:
                // Adobe XD layer: 'Search Bar' (group)
                Stack(
              children: <Widget>[
                // Adobe XD layer: 'Rectangle' (shape)
                Container(
                  width: 352.0,
                  height: 60.0,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5.0),
                    color: const Color(0xffffffff),
                    border:
                        Border.all(width: 1.0, color: const Color(0xffd3e1ff)),
                  ),
                ),
                Transform.translate(
                  offset: Offset(301.5, 19.5),
                  child:
                      // Adobe XD layer: 'Icon' (group)
                      SvgPicture.string(
                    _shapeSVG_9177a39411d746bcae83471437a97c0e,
                    allowDrawingOutsideViewBox: true,
                  ),
                ),
                Transform.translate(
                  offset: Offset(20.0, 21.0),
                  child:
                      // Adobe XD layer: 'Search topic, course' (text)
                      Text(
                    'Search topic, course or instructor',
                    style: TextStyle(
                      fontFamily: 'Manrope-Regular',
                      fontSize: 14,
                      color: const Color(0xcc8a959e),
                    ),
                    textAlign: TextAlign.left,
                  ),
                ),
              ],
            ),
          ),
          Transform.translate(
            offset: Offset(41.0, 120.0),
            child:
                // Adobe XD layer: 'Good Morning!' (text)
                Text(
              'Good Morning!',
              style: TextStyle(
                fontFamily: 'Manrope-Light',
                fontSize: 18,
                color: const Color(0xff3a3a3a),
                letterSpacing: -0.7000000076293945,
              ),
              textAlign: TextAlign.left,
            ),
          ),
          Transform.translate(
            offset: Offset(41.0, 143.0),
            child:
                // Adobe XD layer: 'Arnob Mukherjee' (text)
                Text(
              'Arnob Mukherjee',
              style: TextStyle(
                fontFamily: 'Manrope-Bold',
                fontSize: 30,
                color: const Color(0xff3a3a3a),
                letterSpacing: -0.7000000190734864,
              ),
              textAlign: TextAlign.left,
            ),
          ),
          Transform.translate(
            offset: Offset(32.0, 298.0),
            child:
                // Adobe XD layer: 'Featured Courses' (text)
                Text.rich(
              TextSpan(
                style: TextStyle(
                  fontFamily: 'Manrope-Bold',
                  fontSize: 20,
                  color: const Color(0xff6886c5),
                  letterSpacing: -0.7000000000000001,
                ),
                children: [
                  TextSpan(
                    text: 'Featured',
                  ),
                  TextSpan(
                    text: ' Courses',
                    style: TextStyle(
                      color: const Color(0xff3a3a3a),
                    ),
                  ),
                ],
              ),
              textAlign: TextAlign.left,
            ),
          ),
          Transform.translate(
            offset: Offset(32.0, 629.0),
            child:
                // Adobe XD layer: 'Best Selling Courses' (text)
                Text.rich(
              TextSpan(
                style: TextStyle(
                  fontFamily: 'Manrope-Bold',
                  fontSize: 20,
                  color: const Color(0xff6886c5),
                  letterSpacing: -0.7000000000000001,
                ),
                children: [
                  TextSpan(
                    text: 'Best Selling',
                  ),
                  TextSpan(
                    text: ' Courses',
                    style: TextStyle(
                      color: const Color(0xff3a3a3a),
                    ),
                  ),
                ],
              ),
              textAlign: TextAlign.left,
            ),
          ),
          Transform.translate(
            offset: Offset(337.0, 633.0),
            child:
                // Adobe XD layer: 'View All' (text)
                Text(
              'View All',
              style: TextStyle(
                fontFamily: 'Manrope-Bold',
                fontSize: 14,
                color: const Color(0xffffacb7),
                letterSpacing: -0.49000000000000005,
                decoration: TextDecoration.underline,
              ),
              textAlign: TextAlign.left,
            ),
          ),
          Transform.translate(
            offset: Offset(327.0, 121.0),
            child:
                // Adobe XD layer: 'Bitmap' (group)
                Stack(
              children: <Widget>[
                // Adobe XD layer: 'Mask' (shape)
                Container(
                  width: 53.0,
                  height: 53.0,
                  decoration: BoxDecoration(
                    borderRadius:
                        BorderRadius.all(Radius.elliptical(26.5, 26.5)),
                    color: const Color(0xffd8d8d8),
                  ),
                ),
                // Adobe XD layer: 'Bitmap' (group)
                Stack(
                  children: <Widget>[
                    // Adobe XD layer: 'Bitmap' (shape)
                    Container(
                      width: 53.0,
                      height: 53.0,
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          image: const AssetImage(''),
                          fit: BoxFit.fill,
                        ),
                      ),
                    ),
                    // Adobe XD layer: 'Mask' (shape)
                    Container(
                      width: 53.0,
                      height: 53.0,
                      decoration: BoxDecoration(
                        borderRadius:
                            BorderRadius.all(Radius.elliptical(26.5, 26.5)),
                        color: const Color(0xffd8d8d8),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          Transform.translate(
            offset: Offset(37.0, 36.0),
            child:
                // Adobe XD layer: 'Menu Icon' (group)
                Stack(
              children: <Widget>[
                // Adobe XD layer: 'Rectangle' (shape)
                Container(
                  width: 47.0,
                  height: 47.0,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10.0),
                    color: const Color(0x336886c5),
                  ),
                ),
                Transform.translate(
                  offset: Offset(15.6, 16.8),
                  child:
                      // Adobe XD layer: 'Icon' (group)
                      SvgPicture.string(
                    _shapeSVG_1cc50ef2137b409b8244743589d36fd5,
                    allowDrawingOutsideViewBox: true,
                  ),
                ),
              ],
            ),
          ),
          Transform.translate(
            offset: Offset(333.0, 36.0),
            child:
                // Adobe XD layer: 'Notification Icon' (group)
                Stack(
              children: <Widget>[
                // Adobe XD layer: 'Rectangle' (shape)
                Container(
                  width: 47.0,
                  height: 47.0,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10.0),
                    color: const Color(0x336886c5),
                  ),
                ),
                Transform.translate(
                  offset: Offset(14.25, 14.25),
                  child:
                      // Adobe XD layer: 'Combined Shape' (shape)
                      SvgPicture.string(
                    _shapeSVG_e9f3f29d73f24660a2bef380866ea430,
                    allowDrawingOutsideViewBox: true,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

const String _shapeSVG_0e548a8677b942519d2eb7923e51fce9 =
    '<svg viewBox="152.3 145.0 57.9 35.6" ><g transform="translate(152.0, 172.0)"><g transform=""><g transform=""><path transform="translate(0.28, 0.42)" d="M 8.4375 3.121875047683716 L 5.217187404632568 3.121875047683716 L 4.21875 0 L 3.220312595367432 3.121875047683716 L 0 3.121875047683716 L 2.6015625 5.048437595367432 L 1.6171875 8.15625 L 4.21875 6.229687690734863 L 6.8203125 8.15625 L 5.821875095367432 5.034375190734863 L 8.4375 3.121875047683716 Z" fill="#ffce31" stroke="none" stroke-width="1" stroke-miterlimit="10" stroke-linecap="butt" /></g></g><g transform="translate(12.0, 0.0)"><g transform=""><path transform="translate(0.28, 0.42)" d="M 8.4375 3.121875047683716 L 5.217187404632568 3.121875047683716 L 4.21875 0 L 3.220312595367432 3.121875047683716 L 0 3.121875047683716 L 2.6015625 5.048437595367432 L 1.6171875 8.15625 L 4.21875 6.229687690734863 L 6.8203125 8.15625 L 5.821875095367432 5.034375190734863 L 8.4375 3.121875047683716 Z" fill="#ffce31" stroke="none" stroke-width="1" stroke-miterlimit="10" stroke-linecap="butt" /></g></g><g transform="translate(25.0, 0.0)"><g transform=""><path transform="translate(0.28, 0.42)" d="M 8.4375 3.121875047683716 L 5.217187404632568 3.121875047683716 L 4.21875 0 L 3.220312595367432 3.121875047683716 L 0 3.121875047683716 L 2.6015625 5.048437595367432 L 1.6171875 8.15625 L 4.21875 6.229687690734863 L 6.8203125 8.15625 L 5.821875095367432 5.034375190734863 L 8.4375 3.121875047683716 Z" fill="#ffce31" stroke="none" stroke-width="1" stroke-miterlimit="10" stroke-linecap="butt" /></g></g><g transform="translate(37.0, 0.0)"><g transform=""><path transform="translate(0.28, 0.42)" d="M 8.4375 3.121875047683716 L 5.217187404632568 3.121875047683716 L 4.21875 0 L 3.220312595367432 3.121875047683716 L 0 3.121875047683716 L 2.6015625 5.048437595367432 L 1.6171875 8.15625 L 4.21875 6.229687690734863 L 6.8203125 8.15625 L 5.821875095367432 5.034375190734863 L 8.4375 3.121875047683716 Z" fill="#ffce31" stroke="none" stroke-width="1" stroke-miterlimit="10" stroke-linecap="butt" /></g></g><g transform="translate(49.0, 0.0)"><g transform=""><path transform="translate(0.28, 0.42)" d="M 8.4375 3.121875047683716 L 5.217187404632568 3.121875047683716 L 4.21875 0 L 3.220312595367432 3.121875047683716 L 0 3.121875047683716 L 2.6015625 5.048437595367432 L 1.6171875 8.15625 L 4.21875 6.229687690734863 L 6.8203125 8.15625 L 5.821875095367432 5.034375190734863 L 8.4375 3.121875047683716 Z" fill="#ffce31" stroke="none" stroke-width="1" stroke-miterlimit="10" stroke-linecap="butt" /></g></g></g><path transform="translate(195.0, 145.0)" d="M 14.818359375 2.787903070449829 C 14.58274173736572 2.242342948913574 14.24300384521484 1.747961521148682 13.81816387176514 1.332434177398682 C 13.39300537109375 0.9156673550605774 12.89173221588135 0.5844687819480896 12.34160137176514 0.3568482995033264 C 11.77115058898926 0.1198829635977745 11.15931129455566 -0.001409389544278383 10.54160118103027 1.235544277733425e-05 C 9.675000190734863 1.235544277733425e-05 8.829492568969727 0.2373170405626297 8.0947265625 0.6855592131614685 C 7.9189453125 0.7927857637405396 7.751953125 0.9105592370033264 7.59375 1.03887951374054 C 7.435546875 0.9105592370033264 7.2685546875 0.7927857637405396 7.0927734375 0.6855592131614685 C 6.358007907867432 0.2373170405626297 5.512499809265137 1.235544277733425e-05 4.645898342132568 1.235544277733425e-05 C 4.021874904632568 1.235544277733425e-05 3.417187452316284 0.1195436045527458 2.845898389816284 0.3568482995033264 C 2.2939453125 0.5853639245033264 1.796484351158142 0.9140748381614685 1.369335889816284 1.332434177398682 C 0.9439394474029541 1.747492909431458 0.6041213274002075 2.24199104309082 0.369140625 2.787903070449829 C 0.1248046904802322 3.355676412582397 0 3.958606004714966 0 4.579113960266113 C 0 5.164465427398682 0.1195312514901161 5.774426460266113 0.3568359315395355 6.394934177398682 C 0.555468738079071 6.913488864898682 0.8402343988418579 7.451379776000977 1.2041015625 7.994543552398682 C 1.780664086341858 8.854113578796387 2.573437452316284 9.750597953796387 3.557812452316284 10.65938758850098 C 5.189062595367432 12.16583251953125 6.804491996765137 13.20645809173584 6.873046875 13.24864482879639 L 7.289648532867432 13.51583290100098 C 7.474218845367432 13.63360595703125 7.711523532867432 13.63360595703125 7.896093845367432 13.51583290100098 L 8.312695503234863 13.24864482879639 C 8.381250381469727 13.20469951629639 9.994921684265137 12.16583251953125 11.6279296875 10.65938758850098 C 12.6123046875 9.750597953796387 13.40507793426514 8.854113578796387 13.98164081573486 7.994543552398682 C 14.34550762176514 7.451379776000977 14.63203144073486 6.913488864898682 14.82890605926514 6.394934177398682 C 15.06621074676514 5.774426460266113 15.18574237823486 5.164465427398682 15.18574237823486 4.579113960266113 C 15.1875 3.958606004714966 15.06269550323486 3.355676412582397 14.818359375 2.787903070449829 Z" fill="#ffacb7" stroke="none" stroke-width="1" stroke-miterlimit="10" stroke-linecap="butt" /></svg>';
const String _shapeSVG_9177a39411d746bcae83471437a97c0e =
    '<svg viewBox="301.5 19.5 18.2 18.2" ><g transform="translate(301.0, 19.0)"><path transform="translate(0.5, 0.5)" d="M 16.48530006408691 17.89920043945313 L 12.90509986877441 14.31990051269531 C 9.56879997253418 16.90920066833496 4.794300079345703 16.45919990539551 1.999800086021423 13.29120063781738 C -0.7937999963760376 10.12410068511963 -0.643500030040741 5.329800128936768 2.343600034713745 2.343600034713745 C 5.329800128936768 -0.643500030040741 10.12410068511963 -0.7937999963760376 13.29120063781738 1.999800086021423 C 16.45919990539551 4.794300079345703 16.90920066833496 9.56879997253418 14.31990051269531 12.90509986877441 L 14.32080078125 12.90690040588379 L 17.90010070800781 16.4862003326416 C 18.16020011901855 16.73820114135742 18.26460075378418 17.10989952087402 18.17280006408691 17.4591007232666 C 18.08100128173828 17.80920028686523 17.80739974975586 18.08189964294434 17.45820045471191 18.17280006408691 C 17.37454223632813 18.19473648071289 17.28959655761719 18.20541763305664 17.20541381835938 18.20542144775391 C 16.93729400634766 18.20543670654297 16.67639923095703 18.09714889526367 16.48530006408691 17.89920043945313 Z" fill="#ffacb7" stroke="none" stroke-width="1" stroke-miterlimit="10" stroke-linecap="butt" /></g></svg>';
const String _shapeSVG_1cc50ef2137b409b8244743589d36fd5 =
    '<svg viewBox="15.6 16.8 16.8 14.4" ><g transform="translate(15.0, 16.0)"><path transform="translate(0.6, 0.8)" d="M 1.199699997901917 14.40000057220459 C 0.5372999906539917 14.40000057220459 0 13.86270046234131 0 13.2003002166748 C 0 12.53700065612793 0.5372999906539917 11.99970054626465 1.199699997901917 11.99970054626465 L 15.59969997406006 11.99970054626465 C 16.26300048828125 11.99970054626465 16.80030059814453 12.53700065612793 16.80030059814453 13.2003002166748 C 16.80030059814453 13.86270046234131 16.26300048828125 14.40000057220459 15.59969997406006 14.40000057220459 L 1.199699997901917 14.40000057220459 Z M 1.199699997901917 8.399700164794922 C 0.5372999906539917 8.399700164794922 0 7.862400054931641 0 7.200000286102295 C 0 6.537600040435791 0.5372999906539917 6.00029993057251 1.199699997901917 6.00029993057251 L 8.399700164794922 6.00029993057251 C 9.063000679016113 6.00029993057251 9.600299835205078 6.537600040435791 9.600299835205078 7.200000286102295 C 9.600299835205078 7.862400054931641 9.063000679016113 8.399700164794922 8.399700164794922 8.399700164794922 L 1.199699997901917 8.399700164794922 Z M 1.199699997901917 2.400300025939941 C 0.5372999906539917 2.400300025939941 0 1.86300003528595 0 1.199699997901917 C 0 0.5372999906539917 0.5372999906539917 0 1.199699997901917 0 L 15.59969997406006 0 C 16.26300048828125 0 16.80030059814453 0.5372999906539917 16.80030059814453 1.199699997901917 C 16.80030059814453 1.86300003528595 16.26300048828125 2.400300025939941 15.59969997406006 2.400300025939941 L 1.199699997901917 2.400300025939941 Z" fill="#6886c5" stroke="none" stroke-width="1" stroke-miterlimit="10" stroke-linecap="butt" /></g></svg>';
const String _shapeSVG_e9f3f29d73f24660a2bef380866ea430 =
    '<svg viewBox="14.3 14.3 19.5 19.5" ><path transform="translate(14.25, 14.25)" d="M 2.166300058364868 19.50030136108398 C 0.9720000028610229 19.50030136108398 0 18.52830123901367 0 17.33310127258301 L 0 3.249900102615356 C 0 2.054700136184692 0.9720000028610229 1.083600044250488 2.166300058364868 1.083600044250488 L 11.29050064086914 1.083600044250488 C 10.99890041351318 1.74779999256134 10.83330059051514 2.478600025177002 10.83330059051514 3.249900102615356 L 2.166300058364868 3.249900102615356 L 2.166300058364868 17.33310127258301 L 16.25040054321289 17.33310127258301 L 16.25040054321289 8.666999816894531 C 16.99650001525879 8.66610050201416 17.73360061645508 8.51039981842041 18.41670036315918 8.209799766540527 L 18.41670036315918 17.33310127258301 C 18.41670036315918 18.52830123901367 17.44470024108887 19.50030136108398 16.25040054321289 19.50030136108398 L 2.166300058364868 19.50030136108398 Z M 12.99960041046143 3.249900102615356 C 12.99960041046143 1.455300092697144 14.45490074157715 0 16.25040054321289 0 C 18.04500007629395 0 19.50030136108398 1.455300092697144 19.50030136108398 3.249900102615356 C 19.50030136108398 5.044500350952148 18.04500007629395 6.499800205230713 16.25040054321289 6.499800205230713 C 14.45490074157715 6.499800205230713 12.99960041046143 5.044500350952148 12.99960041046143 3.249900102615356 Z" fill="#6886c5" stroke="none" stroke-width="1" stroke-miterlimit="10" stroke-linecap="butt" /></svg>';
